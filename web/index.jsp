<%--
  Created by IntelliJ IDEA.
  User: Galle
  Date: 2022/4/13
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>欢迎登录index</title>
  </head>
  <body>
  <form action="login" method="post">
    <table align="center">
      <tr>
        <td>用户名：</td>
        <td><input type="text" name="username"></td>
      </tr>
      <tr>
        <td>密 码</td>
        <td><input type="password" name="password"></td>
      </tr>
      <tr>
        <td></td>
        <td align="right">
            <%
            String loginMsg = (String) request.getAttribute("loginMsg");
            if (null != loginMsg){
              out.println("<font color = 'red'>"+loginMsg+"</font>");
            }
          %>
      </tr>
      <tr>
        <td></td>
        <td align="right"><input type="submit" value="登录"></td>
      </tr>
    </table>
  </form>
  </body>
</html>
